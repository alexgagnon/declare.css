# Styleguide

CLI for create a living styleguide for your app

## TODO

- [ ] watch files with chokidar
- [x] use memory-fs as in memory file system
- [ ] auto open index with opn
- [x] serve files with express
- [x] inject src as links in the index with cheerio
- [ ] live reload
- [ ] allow setting of source order and attributes like media
- [ ] support user defined template, config, sources, and other options