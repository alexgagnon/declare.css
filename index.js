#!/usr/bin/env node
'use strict';

const fs = require('fs');
const MemoryFS = require('memory-fs');
const mfs = new MemoryFS();
const yargs = require('yargs');
const chalk = require('chalk');
const emoji = require('node-emoji');
const updateNotifier = require('update-notifier');
const cheerio = require('cheerio');
const chokidar = require('chokidar');
const pkg = require('./package.json');
const {run} = require('./src/server.js');

const DIST_DIR = '/';
const DEFAULT_TEMPLATE = './src/styleguide.html';

updateNotifier({pkg}).notify();

const args = yargs
.usage(
  '$0 [args]',
  `A living style guide for your web app.`
)
.option('c', {
  alias: 'config',
  describe: 'Path to a JSON config file',
  nargs: 1,
  config: configPath => {
    return JSON.parse(fs.readFileSync(configPath, {encoding: 'utf-8'}));
  },
  type: 'string',
  requiresArg: true
})
.option ('n', {
  alias: 'no-open',
  describe: 'Don\'t automatically open a browser window',
  default: false,
  type: 'boolean'
})
.option('s', {
  alias: 'sources',
  describe: 'The list of source css files',
  type: 'array'
})
.option('t', {
  alias: 'template',
  nargs: 1,
  describe: 'Path to an html template file',
  type: 'string',
  requiresArg: true
})
.epilogue('For more information see http://github.com/alexgagnon/styleguide')
.version()
.help()
.argv;

console.log(args);

// load template file
let templateFile;
if (args.template != null) {
  try {
    templateFile = fs.readFileSync(args.template, {encoding: 'utf-8'});
  }
  catch(e) {
    console.error('Could not find template file');
    process.exit(1);
  }
}
else {
  console.log('using default template');
  templateFile = fs.readFileSync(DEFAULT_TEMPLATE, {encoding: 'utf-8'});
}

let sources = ['./node_modules/declare.css/dist/styles.css'];

// use cheerio to inject source styles as links
const $ = cheerio.load(templateFile);
const head = $('head');
sources.forEach(source => {
  head.append(`<link rel='stylesheet' href='${source.split('/').pop()}'>`);
});

try {
  mfs.mkdirpSync(DIST_DIR);
  const index = fs.readFileSync('./src/index.html', {encoding: 'utf-8'});
  mfs.writeFileSync(DIST_DIR + '/index.html', index, {encoding: 'utf-8'});
  mfs.writeFileSync(DIST_DIR + '/styleguide.html', $.html());
  sources.forEach(source => {
    const file = fs.readFileSync(source, {encoding: 'utf-8'});
    mfs.writeFileSync(DIST_DIR + '/' + source.split('/').pop(), file, {encoding: 'utf-8'});
  });
}
catch(e) {
  console.error(e);
}

run(args, mfs);
